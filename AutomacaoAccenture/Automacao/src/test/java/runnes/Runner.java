package runnes;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)
@CucumberOptions(
		features = "src/test/resources/features/solicitacaoSeguroAutomovel.feature",
		glue = "steps"		
)
public class Runner {

}
