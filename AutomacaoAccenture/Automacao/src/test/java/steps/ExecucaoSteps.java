package steps;

import java.util.Arrays;
import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import cucumber.api.java.pt.Dado;
import cucumber.api.java.pt.E;
import cucumber.api.java.pt.Então;
import cucumber.api.java.pt.Quando;
import pages.AbaEnterInsurantDataPage;
import pages.AbaEnterProductDataPage;
import pages.AbaEnterVehicleDataPage;
import pages.AbaSelectPriceOptionPage;
import pages.AbaSendQuotePage;

public class ExecucaoSteps {
	
	protected static WebDriver driver;
	protected String url;
	private AbaEnterVehicleDataPage abaEnterVehicleDataPage = null;
	private AbaEnterInsurantDataPage abaEnterInsurantDataPage = null;
	private AbaEnterProductDataPage abaEnterProductDataPage = null;
	private AbaSelectPriceOptionPage abaSelectPriceOption = null;
	private AbaSendQuotePage abaSendQuotePage = null;

	@Dado("^que ao acessar a página \"([^\"]*)\"$")
	public void acesso_pagina(String site) throws Throwable {
		System.setProperty("webdriver.chrome.driver","D:\\webdriver\\chromedriver.exe");
		ChromeOptions options = new ChromeOptions();
		options.addArguments("--start-maximized");
		driver = new ChromeDriver(options);		
		driver.get(site);
		url = site;
	}
	
	@Quando("^selecionar a opção \"([^\"]*)\" no campo Make$")
	public void selecionar_make(String make) throws Throwable {
		abaEnterVehicleDataPage = new AbaEnterVehicleDataPage(driver);
		abaEnterVehicleDataPage.selecionarCampoMake(make);
	}
	
	@E("^selecionar a opção \"([^\"]*)\" no campo Model$")
	public void selecionar_model(String model) throws Throwable {
		abaEnterVehicleDataPage.selecionarCampoModel(model);
	}
	
	@E("^informar o valor \"([^\"]*)\" no campo Cylinder Capacity$")
	public void informar_cylinder_capacity(String cylinderCapacity) throws Throwable {
		abaEnterVehicleDataPage.preencherCampoCylinderCapacity(cylinderCapacity);
	}
	
	@E("^informar o valor \"([^\"]*)\" no campo Engine Performance$")
	public void informar_engine_performance(String enginePerformance) throws Throwable {
		abaEnterVehicleDataPage.preencherCampoEnginePerformance(enginePerformance);
	}
	
	@E("^informar o valor \"([^\"]*)\" no campo Date of Manufacture$")
	public void informar_date_of_manufacture(String dateOfManufacture) throws Throwable {
		abaEnterVehicleDataPage.preencherCampoDateOfManufacture(dateOfManufacture);
	}
	
	@E("^selecionar a opção \"([^\"]*)\" no campo Number of Seats$")
	public void selecionar_number_of_seats(String numberOfSeats) throws Throwable {
		abaEnterVehicleDataPage.selecionarCampoNumberOfSeats(numberOfSeats);
	}
	
	@E("^selecionar a opção \"([^\"]*)\" no campo Right Hand Drive$")
	public void selecionar_right_hand_drive(String rightHandDrive) throws Throwable {
		abaEnterVehicleDataPage.selecionarCampoRightHandDrive(rightHandDrive);
	}	
	
	@E("^selecionar a opção \"([^\"]*)\" no campo Number of Seats Motor Cycle$")
	public void selecionar_number_of_seats_motor_cycle(String numberOfSeatsMotorCycle) throws Throwable {
		abaEnterVehicleDataPage.selecionarCampoNumberOfSeatsMotorCycle(numberOfSeatsMotorCycle);
	}
	
	@E("^selecionar a opção \"([^\"]*)\" no campo Fuel Type$")
	public void selecionar_fuel_type(String fuelType) throws Throwable {
		abaEnterVehicleDataPage.selecionarCampoFuelType(fuelType);
	}
	
	@E("^informar o valor \"([^\"]*)\" no campo Payload$")
	public void informar_payload(String payload) throws Throwable {
		abaEnterVehicleDataPage.preencherCampoPayload(payload);
	}
	
	@E("^informar o valor \"([^\"]*)\" no campo Total Weight$")
	public void informar_total_weight(String totalWeight) throws Throwable {
		abaEnterVehicleDataPage.preencherCampoTotalWeight(totalWeight);
	}
	
	@E("^informar o valor \"([^\"]*)\" no campo List Price$")
	public void informar_list_price(String listPrice) throws Throwable {
		abaEnterVehicleDataPage.preencherCampoListPrice(listPrice);
	}
	
	@E("^informar o valor \"([^\"]*)\" no campo License Plate Number$")
	public void informar_license_plate_number(String licensePlateNumber) throws Throwable {
		abaEnterVehicleDataPage.preencherCampoLicensePlateNumber(licensePlateNumber);
	}
	
	@E("^informar o valor \"([^\"]*)\" no campo Annual Mileage$")
	public void informar_annual_mileage(String annualMileage) throws Throwable {
		abaEnterVehicleDataPage.preencherCampoAnnualMileage(annualMileage);
	}
	
	@Então("^o contador de preenchimento da aba Enter Vehicle Data está com o valor zero$")
	public void valida_contador() throws Throwable {
		abaEnterVehicleDataPage.validaContadorPreenchimentoAba();
	}
	
	@Dado("^que ao clicar no botão Next 1$")
	public void clicar_botao_next_1() throws Throwable {
		abaEnterInsurantDataPage = new AbaEnterInsurantDataPage(driver);
		abaEnterInsurantDataPage.clicarBotaoNext();
	}	

	@Quando("^informar o valor \"([^\"]*)\" no campo First Name$")
	public void informar_first_name(String firstName) throws Throwable {		
		abaEnterInsurantDataPage.preencherCampoFirstName(firstName);
	}
	
	@E("^informar o valor \"([^\"]*)\" no campo Last Name$")
	public void informar_last_name(String lastName) throws Throwable {
		abaEnterInsurantDataPage.preencherCampoLastName(lastName);
	}
	
	@E("^informar o valor \"([^\"]*)\" no campo Date of Birth$")
	public void informar_date_of_birth(String birthDate) throws Throwable {
		abaEnterInsurantDataPage.preencherCampoBirthDate(birthDate);
	}
	
	@E("^selecionar a opção \"([^\"]*)\" no campo Gender$")
	public void selecionar_gender(String gender) throws Throwable {
		abaEnterInsurantDataPage.selecionarCampoGender(gender);
	}
	
	@E("^informar o valor \"([^\"]*)\" no campo Street Address$")
	public void informar_street_address(String streetAddress) throws Throwable {
		abaEnterInsurantDataPage.preencherCampoStreetAddress(streetAddress);
	}
	
	@E("^selecionar a opção \"([^\"]*)\" no campo Country$")
	public void selecionar_country(String country) throws Throwable {
		abaEnterInsurantDataPage.selecionarCampoCountry(country);
	}
	
	@E("^informar o valor \"([^\"]*)\" no campo Zip Code$")
	public void informar_zip_code(String zipCode) throws Throwable {
		abaEnterInsurantDataPage.preencherCampoZipCode(zipCode);
	}
	
	@E("^informar o valor \"([^\"]*)\" no campo City$")
	public void informar_city(String city) throws Throwable {
		abaEnterInsurantDataPage.preencherCampoCity(city);
	}
	
	@E("^selecionar a opção \"([^\"]*)\" no campo Occupation$")
	public void selecionar_occupation(String occupation) throws Throwable {
		abaEnterInsurantDataPage.selecionarCampoOccupation(occupation);
	}
	
	@E("^selecionar o valor \"([^\"]*)\" no campo Hobbies$")
	public void selecionar_lista_hobbies(String todosOsHobbies) throws Throwable {
		List<String> listaHobbies = Arrays.asList(todosOsHobbies.split(","));
		abaEnterInsurantDataPage.selecionarCampoHobbies(listaHobbies);
	}
	
	@E("^informar o valor \"([^\"]*)\" no campo Website$")
	public void informar_website(String website) throws Throwable {
		abaEnterInsurantDataPage.preencherCampoWebsite(website);
	}
	
	@Então("^o contador de preenchimento da aba Enter Insurant Data está com o valor zero$")
	public void valida_contador_enter_insurant_data() throws Throwable {
		abaEnterInsurantDataPage.validaContadorPreenchimentoAba();
	}	
	
	@Dado("^que ao clicar no botão Next 2$")
	public void clicar_botao_next_2() throws Throwable {
		abaEnterProductDataPage = new AbaEnterProductDataPage(driver);
		abaEnterProductDataPage.clicarBotaoNext();
	}	

	@Quando("^informar o valor \"([^\"]*)\" no campo Start Date$")
	public void informar_start_date(String startDate) throws Throwable {		
		abaEnterProductDataPage.preencherCampoStartDate(startDate);
	}
	
	@E("^selecionar o valor \"([^\"]*)\" no campo Insurance Sum$")
	public void selecionar_insurance_sum(String insuranceSum) throws Throwable {
		abaEnterProductDataPage.selecionarCampoInsuranceSum(insuranceSum);
	}
	
	@E("^selecionar a opção \"([^\"]*)\" no campo Damage Insurance$")
	public void selecionar_damage_insurance(String damageInsurance) throws Throwable {
		abaEnterProductDataPage.selecionarCampoDamageInsurance(damageInsurance);
	}
	
	@E("^selecionar o valor \"([^\"]*)\" no campo Merit Rating$")
	public void selecionar_merit_rating(String meritRating) throws Throwable {
		abaEnterProductDataPage.selecionarCampoMeritRating(meritRating);
	}
	
	@E("^selecionar os itens \"([^\"]*)\" no campo Optional Products$")
	public void selecionar_optional_products(String optionalProducts) throws Throwable {
		List<String> listaOptionalProducts = Arrays.asList(optionalProducts.split(","));
		abaEnterProductDataPage.selecionarCampoOptionalProducts(listaOptionalProducts);
	}
	
	@E("^selecionar a opção \"([^\"]*)\" no campo Courtesy Car$")
	public void selecionar_courtesy_car(String courtesyCar) throws Throwable {
		abaEnterProductDataPage.selecionarCampoCourtesyCar(courtesyCar);
	}
	
	@Então("^o contador de preenchimento da aba Enter Product Data está com o valor zero$")
	public void valida_contador_enter_product_data() throws Throwable {
		abaEnterProductDataPage.validaContadorPreenchimentoAba();
	}
	
	@Dado("^que ao clicar no botão Next 3$")
	public void clicar_botao_next_3() throws Throwable {
		abaSelectPriceOption = new AbaSelectPriceOptionPage(driver);
		abaSelectPriceOption.clicarBotaoNext();
	}	

	@Quando("^selecionar a opção de valor \"([^\"]*)\" no campo Select Option$")
	public void informar_selecion_price(String selecionPrice) throws Throwable {		
		abaSelectPriceOption.selecionarCampoSelecionPrice(selecionPrice);
	}
	
	@Então("^o contador de preenchimento da aba Select Price Option está com o valor zero$")
	public void valida_contador_select_price_option() throws Throwable {
		abaSelectPriceOption.validaContadorPreenchimentoAba();
	}
	
	@Dado("^que ao clicar no botão Next 4$")
	public void clicar_botao_next_4() throws Throwable {
		abaSendQuotePage = new AbaSendQuotePage(driver);
		abaSendQuotePage.clicarBotaoNext();
	}
	
	@Quando("^informar o valor \"([^\"]*)\" no campo E-Mail$")
	public void informar_email(String email) throws Throwable {		
		abaSendQuotePage.preencherCampoEmail(email);
	}
	
	@E("^informar o valor \"([^\"]*)\" no campo Phone$")
	public void informar_phone(String phone) throws Throwable {
		abaSendQuotePage.preencherCampoPhone(phone);
	}
	
	@E("^informar o valor \"([^\"]*)\" no campo Username$")
	public void informar_username(String username) throws Throwable {
		abaSendQuotePage.preencherCampoUsername(username);
	}
	
	@E("^informar o valor \"([^\"]*)\" no campo Password$")
	public void informar_password(String password) throws Throwable {
		abaSendQuotePage.preencherCampoPassword(password);
	}
	
	@E("^informar o valor \"([^\"]*)\" no campo Confirm Password$")
	public void informar_confirm_password(String confirmPassword) throws Throwable {
		abaSendQuotePage.preencherCampoConfirmPassword(confirmPassword);
	}
	
	@E("^informar o valor \"([^\"]*)\" no campo Comments$")
	public void informar_comments(String comments) throws Throwable {
		abaSendQuotePage.preencherCampoComments(comments);
	}
	
	@E("^clicar no botão Send$")
	public void clicar_botao_send() throws Throwable {
		abaSendQuotePage.clicarBotaoSend();
	}
	
	@Então("^a mensagem \"([^\"]*)\" foi exibida na tela$")
	public void valida_mensagem_final(String mensagem) throws Throwable {
		abaSendQuotePage.validaMensagemSucesso(mensagem);
		finaliza();
	}
		
	public void finaliza() 
	{
		driver.quit();
		System.exit(0);
	}
}
