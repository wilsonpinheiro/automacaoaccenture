package pages;

import static org.junit.Assert.assertEquals;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class AbaSendQuotePage extends BasePage 
{	
	private By buttonNextBy = By.xpath("//*[@id=\"nextsendquote\"]");
	private By emailBy = By.id("email");
	private By phoneBy = By.id("phone");
	private By usernameBy = By.id("username");
	private By passwordBy = By.id("password");
	private By confirmPasswordBy = By.id("confirmpassword");
	private By commentsBy = By.id("Comments");
	private By buttonSendEmailBy = By.id("sendemail");
	private By mensagemBy = By.cssSelector("body > div.sweet-alert.showSweetAlert.visible > h2");
	private By mensagemAlertaBy = By.cssSelector("body > div.sweet-alert.showSweetAlert.visible");
	
	public AbaSendQuotePage(WebDriver driver) {
		super(driver);
	}
	
	public AbaSendQuotePage clicarBotaoNext(){
		WebElement element = retornaElemento(buttonNextBy);	
		element.click();
		return this;
	}
	
	public AbaSendQuotePage preencherCampoEmail(String email){
		WebElement element = retornaElemento(emailBy);	
		element.sendKeys(email);
		return this;
	}
	
	public AbaSendQuotePage preencherCampoUsername(String username){
		WebElement element = retornaElemento(usernameBy);	
		element.sendKeys(username);
		return this;
	}
	
	public AbaSendQuotePage preencherCampoPhone(String phone){
		WebElement element = retornaElemento(phoneBy);	
		element.sendKeys(phone);
		return this;
	}
	
	public AbaSendQuotePage preencherCampoPassword(String password){
		WebElement element = retornaElemento(passwordBy);	
		element.sendKeys(password);
		return this;
	}
	
	public AbaSendQuotePage preencherCampoConfirmPassword(String confirmPassword){
		WebElement element = retornaElemento(confirmPasswordBy);	
		element.sendKeys(confirmPassword);
		return this;
	}
	
	public AbaSendQuotePage preencherCampoComments(String comments){
		WebElement element = retornaElemento(commentsBy);	
		element.sendKeys(comments);
		return this;
	}
	
	public AbaSendQuotePage clicarBotaoSend(){
		WebElement element = retornaElemento(buttonSendEmailBy);	
		element.click();
		return this;
	}
	
	public AbaSendQuotePage validaMensagemSucesso(String mensagem){
		String msg = null;
		WebElement mensagemAlerta = retornaElemento(mensagemAlertaBy);
		
		if (mensagemAlerta.isDisplayed())
		{
			WebElement element = retornaElemento(mensagemBy);
			
			if (element != null)
			{
				msg = element.getText();
			}	
		}
		
		assertEquals(msg, mensagem);
		
		return this;
	}
}
