package pages;

import static org.junit.Assert.assertEquals;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class AbaSelectPriceOptionPage extends BasePage {
	
	private By buttonNextBy = By.xpath("//*[@id=\"nextselectpriceoption\"]");	
	private By priceSilverBy = By.xpath("//*[@id=\"priceTable\"]/tfoot/tr/th[2]/label[1]");
	private By priceGoldBy = By.xpath("//*[@id=\"priceTable\"]/tfoot/tr/th[2]/label[2]");
	private By pricePlatinumBy = By.xpath("//*[@id=\"priceTable\"]/tfoot/tr/th[2]/label[3]");
	private By priceUltimateBy = By.xpath("//*[@id=\"priceTable\"]/tfoot/tr/th[2]/label[4]");
	private By contadorPreenchimentoBy = By.xpath("//*[@id=\"selectpriceoption\"]/span");
	
	public AbaSelectPriceOptionPage(WebDriver driver) {
		super(driver);
	}
	
	public AbaSelectPriceOptionPage clicarBotaoNext(){
		WebElement element = retornaElemento(buttonNextBy);	
		element.click();
		return this;
	}	
	
	public AbaSelectPriceOptionPage selecionarCampoSelecionPrice(String selecionPrice){
		WebElement label = null;
		
		if ("Silver".equals(selecionPrice))
		{
			label = retornaElemento(priceSilverBy);
		}
		else if ("Gold".equals(selecionPrice))
		{
			label = retornaElemento(priceGoldBy);
		}
		else if ("Platinum".equals(selecionPrice))
		{
			label = retornaElemento(pricePlatinumBy);
		}
		else if ("Ultimate".equals(selecionPrice))
		{
			label = retornaElemento(priceUltimateBy);
		}
						
		label.click();

		return this;
	}
		
	public AbaSelectPriceOptionPage validaContadorPreenchimentoAba(){
		int contador = -1;
		WebElement element = retornaElemento(contadorPreenchimentoBy);
		
		if (element != null)
		{
			contador = Integer.parseInt(element.getText());
		}		
		
		assertEquals(contador, 0);
		
		return this;
	}

}