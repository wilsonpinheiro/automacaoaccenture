package pages;

import static org.junit.Assert.assertEquals;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

public class AbaEnterInsurantDataPage extends BasePage {
	
	private By buttonNextBy = By.xpath("//*[@id=\"nextenterinsurantdata\"]");	
	private By firstNameBy = By.id("firstname");
	private By lastNameBy = By.id("lastname");
	private By birthDateBy = By.id("birthdate");
	private By genderMaleBy = By.xpath("//*[@id=\"insurance-form\"]/div/section[2]/div[4]/p/label[1]");
	private By genderFemaleBy = By.xpath("//*[@id=\"insurance-form\"]/div/section[2]/div[4]/p/label[2]");
	private By streetAddressBy = By.id("streetaddress");
	private By countryBy = By.id("country");
	private By zipCodeBy = By.id("zipcode");
	private By cityBy = By.id("city");
	private By occupationBy = By.id("occupation");	
	private By hobbiesSpeedingBy = By.xpath("//*[@id=\"insurance-form\"]/div/section[2]/div[10]/p/label[1]");	
	private By hobbiesBungeeJumpingBy = By.xpath("//*[@id=\"insurance-form\"]/div/section[2]/div[10]/p/label[2]");
	private By hobbiesCliffDivingBy = By.xpath("//*[@id=\"insurance-form\"]/div/section[2]/div[10]/p/label[3]");
	private By hobbiesSkydivingBy = By.xpath("//*[@id=\"insurance-form\"]/div/section[2]/div[10]/p/label[4]");
	private By hobbiesOtherBy = By.xpath("//*[@id=\"insurance-form\"]/div/section[2]/div[10]/p/label[5]");	
	private By websiteBy = By.id("website");	
	private By contadorPreenchimentoBy = By.xpath("//*[@id=\"enterinsurantdata\"]/span");
	
	public AbaEnterInsurantDataPage(WebDriver driver) {
		super(driver);
	}
	
	public AbaEnterInsurantDataPage clicarBotaoNext(){
		WebElement element = retornaElemento(buttonNextBy);	
		element.click();
		return this;
	}
	
	public AbaEnterInsurantDataPage preencherCampoFirstName(String firstName){
		WebElement element = retornaElemento(firstNameBy);	
		element.sendKeys(firstName);
		return this;
	}
	
	public AbaEnterInsurantDataPage preencherCampoLastName(String lastName){
		WebElement element = retornaElemento(lastNameBy);	
		element.sendKeys(lastName);
		return this;
	}
	
	public AbaEnterInsurantDataPage preencherCampoBirthDate(String birthDate){
		WebElement element = retornaElemento(birthDateBy);	
		element.sendKeys(birthDate);
		return this;
	}
	
	public AbaEnterInsurantDataPage selecionarCampoGender(String gender){
		WebElement label = null;
		
		if ("Male".equals(gender))
		{
			label = retornaElemento(genderMaleBy);
		}
		else if ("Female".equals(gender))
		{
			label = retornaElemento(genderFemaleBy);
		}
		
		label.click();
		
		return this;
	}
	
	public AbaEnterInsurantDataPage preencherCampoStreetAddress(String streetAddress){
		WebElement element = retornaElemento(streetAddressBy);	
		element.sendKeys(streetAddress);
		return this;
	}
	
	public AbaEnterInsurantDataPage selecionarCampoCountry(String country){
		WebElement comboboxElement = retornaElemento(countryBy);	
		Select combobox = new Select(comboboxElement);
		combobox.selectByVisibleText(country);
		return this;
	}
	
	public AbaEnterInsurantDataPage preencherCampoZipCode(String zipCode){
		WebElement element = retornaElemento(zipCodeBy);	
		element.sendKeys(zipCode);
		return this;
	}
	
	public AbaEnterInsurantDataPage preencherCampoCity(String city){
		WebElement element = retornaElemento(cityBy);	
		element.sendKeys(city);
		return this;
	}
	
	public AbaEnterInsurantDataPage selecionarCampoOccupation(String occupation)
	{
		WebElement element = retornaElemento(occupationBy);		
		Select combobox = new Select(element);
		combobox.selectByVisibleText(occupation);
		return this;
	}	
	
	public AbaEnterInsurantDataPage selecionarCampoHobbies(List<String> listaHobbies){
		WebElement label = null;
		
		for (int i=0; i < listaHobbies.size(); i++)
		{	
			if ("Speeding".equals(listaHobbies.get(i)))
			{
				label = retornaElemento(hobbiesSpeedingBy);
			}
			else if ("Bungee Jumping".equals(listaHobbies.get(i)))
			{
				label = retornaElemento(hobbiesBungeeJumpingBy);
			}
			else if ("Cliff Diving".equals(listaHobbies.get(i)))
			{
				label = retornaElemento(hobbiesCliffDivingBy);
			}
			else if ("Skydiving".equals(listaHobbies.get(i)))
			{
				label = retornaElemento(hobbiesSkydivingBy);
			}
			else if ("Other".equals(listaHobbies.get(i)))
			{
				label = retornaElemento(hobbiesOtherBy);
			}
						
			label.click();
		}
		return this;
	}
	
	public AbaEnterInsurantDataPage preencherCampoWebsite(String website){
		WebElement element = retornaElemento(websiteBy);
		element.sendKeys(website);
		return this;
	} 
		
	public AbaEnterInsurantDataPage validaContadorPreenchimentoAba(){
		int contador = -1;
		WebElement element = retornaElemento(contadorPreenchimentoBy);
		
		if (element != null)
		{
			contador = Integer.parseInt(element.getText());
		}		
		
		assertEquals(contador, 0);
		
		return this;
	}

}