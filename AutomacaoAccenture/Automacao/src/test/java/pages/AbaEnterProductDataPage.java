package pages;

import static org.junit.Assert.assertEquals;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

public class AbaEnterProductDataPage extends BasePage 
{	
	private By buttonNextBy = By.xpath("//*[@id=\"nextenterproductdata\"]");
	private By startDateBy = By.id("startdate");
	private By insuranceSumBy = By.id("insurancesum");
	private By meritRatingBy = By.id("meritrating");
	private By damageInsuranceBy = By.id("damageinsurance");
	private By euroProtectionBy = By.xpath("//*[@id=\"insurance-form\"]/div/section[3]/div[5]/p/label[1]");
	private By legalDefenseInsurance = By.xpath("//*[@id=\"insurance-form\"]/div/section[3]/div[5]/p/label[2]");
	private By courtesyCarBy = By.id("courtesycar");
	private By contadorPreenchimentoBy = By.xpath("//*[@id=\"enterproductdata\"]/span");
	
	public AbaEnterProductDataPage(WebDriver driver) {
		super(driver);
	}
	
	public AbaEnterProductDataPage clicarBotaoNext(){
		WebElement element = retornaElemento(buttonNextBy);	
		element.click();
		return this;
	}
	
	public AbaEnterProductDataPage preencherCampoStartDate(String startDate){
		WebElement element = retornaElemento(startDateBy);	
		element.sendKeys(startDate);
		return this;
	}
	
	public AbaEnterProductDataPage selecionarCampoInsuranceSum(String insuranceSum){
		WebElement comboboxElement = retornaElemento(insuranceSumBy);	
		Select combobox = new Select(comboboxElement);
		combobox.selectByVisibleText(insuranceSum);
		return this;
	}
	
	public AbaEnterProductDataPage selecionarCampoMeritRating(String meritRating){
		WebElement comboboxElement = retornaElemento(meritRatingBy);	
		Select combobox = new Select(comboboxElement);
		combobox.selectByVisibleText(meritRating);
		return this;
	}
	
	public AbaEnterProductDataPage selecionarCampoDamageInsurance(String damageInsurance)
	{
		WebElement element = retornaElemento(damageInsuranceBy);		
		Select combobox = new Select(element);
		combobox.selectByVisibleText(damageInsurance);
		return this;
	}
	
	public AbaEnterProductDataPage selecionarCampoOptionalProducts(List<String> listaOptionalProducts){
		WebElement label = null;
		
		for (int i=0; i < listaOptionalProducts.size(); i++)
		{	
			if ("Euro Protection".equals(listaOptionalProducts.get(i)))
			{
				label = retornaElemento(euroProtectionBy);
			}
			else if ("Legal Defense Insurance".equals(listaOptionalProducts.get(i)))
			{
				label = retornaElemento(legalDefenseInsurance);
			}
						
			label.click();
		}
		return this;
	}
	
	public AbaEnterProductDataPage selecionarCampoCourtesyCar(String courtesyCar)
	{
		WebElement element = retornaElemento(courtesyCarBy);		
		Select combobox = new Select(element);
		combobox.selectByValue(courtesyCar);
		return this;
	}
	
	public AbaEnterProductDataPage validaContadorPreenchimentoAba(){
		int contador = -1;
		WebElement element = retornaElemento(contadorPreenchimentoBy);
		
		if (element != null)
		{
			contador = Integer.parseInt(element.getText());
		}		
		
		assertEquals(contador, 0);
		
		return this;
	}
}
