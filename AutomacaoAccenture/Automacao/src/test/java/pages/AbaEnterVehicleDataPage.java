package pages;

import static org.junit.Assert.assertEquals;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

public class AbaEnterVehicleDataPage extends BasePage 
{	
	private By makeBy = By.id("make");
	private By modelBy = By.id("model");
	private By cylinderCapacityBy = By.id("cylindercapacity");
	private By enginePerformanceBy = By.id("engineperformance");
	private By dateOfManufactureBy = By.id("dateofmanufacture");
	private By numberOfSeatsBy = By.id("numberofseats");
	private By rightHandDriveYesBy = By.xpath("//*[@id=\"insurance-form\"]/div/section[1]/div[7]/p/label[1]");
	private By rightHandDriveNoBy = By.xpath("//*[@id=\"insurance-form\"]/div/section[1]/div[7]/p/label[2]");
	private By numberOfSeatsMotorCycleBy = By.id("numberofseatsmotorcycle");
	private By fuelBy = By.id("fuel");
	private By payloadBy = By.id("payload");
	private By totalWeightBy = By.id("totalweight");
	private By listPriceBy = By.id("listprice");
	private By licensePlateNumberBy = By.id("licenseplatenumber");
	private By annualMileageBy = By.id("annualmileage");
	private By contadorPreenchimentoBy = By.xpath("//*[@id=\"entervehicledata\"]/span");
	
	public AbaEnterVehicleDataPage(WebDriver driver) {
		super(driver);
	}
	
	public AbaEnterVehicleDataPage selecionarCampoMake(String make){
		WebElement comboboxElement = retornaElemento(makeBy);	
		Select combobox = new Select(comboboxElement);
		combobox.selectByVisibleText(make);
		return this;
	}
	
	public AbaEnterVehicleDataPage selecionarCampoModel(String model)
	{
		WebElement element = retornaElemento(modelBy);		
		Select combobox = new Select(element);
		combobox.selectByVisibleText(model);
		return this;
	}
	
	public AbaEnterVehicleDataPage preencherCampoCylinderCapacity(String cylinderCapacity){
		WebElement element = retornaElemento(cylinderCapacityBy);	
		element.sendKeys(cylinderCapacity);
		return this;
	}
	
	public AbaEnterVehicleDataPage preencherCampoEnginePerformance(String enginePerformance){
		WebElement element = retornaElemento(enginePerformanceBy);	
		element.sendKeys(enginePerformance);
		return this;
	}
	
	public AbaEnterVehicleDataPage preencherCampoDateOfManufacture(String dateOfManufacture){
		WebElement element = retornaElemento(dateOfManufactureBy);	
		element.sendKeys(dateOfManufacture);
		return this;
	}
		
	public AbaEnterVehicleDataPage selecionarCampoNumberOfSeats(String numberOfSeats){
		WebElement comboboxElement = retornaElemento(numberOfSeatsBy);	
		Select combobox = new Select(comboboxElement);
		combobox.selectByVisibleText(numberOfSeats);
		return this;
	}
	
	public AbaEnterVehicleDataPage selecionarCampoRightHandDrive(String rightHandDrive){
		WebElement label = null;
		
		if ("Yes".equals(rightHandDrive))
		{
			label = retornaElemento(rightHandDriveYesBy);
		}
		else if ("No".equals(rightHandDrive))
		{
			label = retornaElemento(rightHandDriveNoBy);
		}
		
		label.click();
		
		return this;
	}
	
	public AbaEnterVehicleDataPage selecionarCampoNumberOfSeatsMotorCycle(String numberOfSeatsMotorCycle){
		WebElement comboboxElement = retornaElemento(numberOfSeatsMotorCycleBy);
		Select combobox = new Select(comboboxElement);
		combobox.selectByVisibleText(numberOfSeatsMotorCycle);
		return this;
	}
	
	public AbaEnterVehicleDataPage selecionarCampoFuelType(String fuelType){
		WebElement comboboxElement  = retornaElemento(fuelBy);
		Select combobox = new Select(comboboxElement);
		combobox.selectByVisibleText(fuelType);
		return this;
	}
	
	public AbaEnterVehicleDataPage preencherCampoPayload(String payload){
		WebElement element = retornaElemento(payloadBy);
		element.sendKeys(payload);
		return this;
	} 
	
	public AbaEnterVehicleDataPage preencherCampoTotalWeight(String totalWeight){
		WebElement element = retornaElemento(totalWeightBy);
		element.sendKeys(totalWeight);
		return this;
	} 
	
	public AbaEnterVehicleDataPage preencherCampoListPrice(String listPrice){
		WebElement element = retornaElemento(listPriceBy);
		element.sendKeys(listPrice);
		return this;
	}
	
	public AbaEnterVehicleDataPage preencherCampoLicensePlateNumber(String licensePlateNumber){
		WebElement element = retornaElemento(licensePlateNumberBy);
		element.sendKeys(licensePlateNumber);
		return this;
	}
	
	public AbaEnterVehicleDataPage preencherCampoAnnualMileage(String annualMileage){
		WebElement element = retornaElemento(annualMileageBy);
		element.sendKeys(annualMileage);
		return this;
	}
	
	public AbaEnterVehicleDataPage validaContadorPreenchimentoAba(){
		int contador = -1;
		WebElement element = retornaElemento(contadorPreenchimentoBy);
		
		if (element != null)
		{
			contador = Integer.parseInt(element.getText());
		}		
		
		assertEquals(contador, 0);
		
		return this;
	}
}
