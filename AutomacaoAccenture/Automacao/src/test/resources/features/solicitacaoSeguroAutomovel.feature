# language: pt
@step
Funcionalidade: Solicitação de Seguro de Automóvel
  Eu como cliente da seguradora desejo realizar a solicitação de cotação de seguro
  para o meu veículo

Contexto:
  @step1 
  Esquema do Cenário: Preencher aba Enter Vehicle Data
    Dado que ao acessar a página "http://sampleapp.tricentis.com/101/app.php"
    Quando selecionar a opção <make> no campo Make
    E selecionar a opção <model> no campo Model
    E informar o valor <cylinder capacity> no campo Cylinder Capacity
    E informar o valor <engine performance> no campo Engine Performance
    E informar o valor <date of manufacture> no campo Date of Manufacture
    E selecionar a opção <number of seats> no campo Number of Seats
    E selecionar a opção <right hand drive> no campo Right Hand Drive
    E selecionar a opção <number of seats motor cycle> no campo Number of Seats Motor Cycle
    E selecionar a opção <fuel type> no campo Fuel Type
    E informar o valor <payload> no campo Payload
    E informar o valor <total weight> no campo Total Weight   
    E informar o valor <list price> no campo List Price
    E informar o valor <license plate number> no campo License Plate Number
    E informar o valor <annual mileage> no campo Annual Mileage
    Então o contador de preenchimento da aba Enter Vehicle Data está com o valor zero

    Exemplos: 
      | make     | model        | cylinder capacity | engine performance | date of manufacture |  number of seats | right hand drive | number of seats motor cycle | fuel type | payload | total weight | list price | license plate number | annual mileage |
      | "BMW"    | "Motorcycle" | "1000"            | "2000"             | "01/12/2019"        |  "2"             | "No"             | "2"                         | "Petrol"  | "150"   | "250"        | "22000"      | "ABC1234"          | "10000"        |
	
	@step2
  Esquema do Cenário: Preencher aba Enter Insurant Data
    Dado que ao clicar no botão Next 1
    Quando informar o valor <first name> no campo First Name
    E informar o valor <last name> no campo Last Name
    E informar o valor <date of birth> no campo Date of Birth
    E selecionar a opção <gender> no campo Gender
    E informar o valor <street address> no campo Street Address
    E selecionar a opção <country> no campo Country
    E informar o valor <zip code> no campo Zip Code
    E informar o valor <city> no campo City
    E selecionar a opção <occupation> no campo Occupation
    E selecionar o valor <hobbies> no campo Hobbies
    E informar o valor <website> no campo Website
    Então o contador de preenchimento da aba Enter Insurant Data está com o valor zero

    Exemplos: 
      | first name | last name  | date of birth | gender | date of manufacture |  street address | country  | zip code   | city       | occupation | hobbies                   | website           |
      | "Wilson"   | "Pinheiro" | "08/01/1980"  | "Male" | "01/12/2019"        |  "Rua Teste"    | "Brazil" | "86055250" | "Londrina" | "Employee" | "Skydiving,Cliff Diving" | "www.teste.com.br" |
      
  @step3
  Esquema do Cenário: Preencher aba Enter Product Data
    Dado que ao clicar no botão Next 2
    Quando informar o valor <start date> no campo Start Date
    E selecionar o valor <insurance sum> no campo Insurance Sum
    E selecionar o valor <merit rating> no campo Merit Rating
    E selecionar a opção <damage insurance> no campo Damage Insurance
    E selecionar os itens <optional products> no campo Optional Products
    E selecionar a opção <courtesy car> no campo Courtesy Car
    Então o contador de preenchimento da aba Enter Product Data está com o valor zero

    Exemplos: 
      | start date   | insurance sum   | merit rating | damage insurance | optional products | courtesy car |
      | "01/01/2021" | "3.000.000,00" | "Bonus 1"     | "No Coverage"    | "Euro Protection" | "Yes"        |
      
  @step4
  Esquema do Cenário: Preencher aba Select Price Option
    Dado que ao clicar no botão Next 3
    Quando selecionar a opção de valor <select option> no campo Select Option
    Então o contador de preenchimento da aba Select Price Option está com o valor zero

    Exemplos: 
      | select option |
      | "Ultimate"    |
      
  @step5
  Esquema do Cenário: Preencher aba Send Quote
    Dado que ao clicar no botão Next 4
    Quando informar o valor <email> no campo E-Mail
    E informar o valor <phone> no campo Phone
    E informar o valor <username> no campo Username
    E informar o valor <password> no campo Password
    E informar o valor <confirm password> no campo Confirm Password
    E informar o valor <comments> no campo Comments
    E clicar no botão Send
    Então a mensagem "Sending e-mail success!" foi exibida na tela 

    Exemplos: 
      | email             | phone         | username    | password     | confirm password | comments              |
      | "teste@gmail.com" | "43999680503" | "wpinheiro" | "Teste@2020" | "Teste@2020"     | "Teste de comentário" |